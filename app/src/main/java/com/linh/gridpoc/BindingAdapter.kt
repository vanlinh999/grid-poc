package com.linh.gridpoc

import android.content.Context
import android.graphics.drawable.GradientDrawable
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.widget.FrameLayout
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import kotlin.math.roundToInt

@BindingAdapter("outerBorderRadiusType")
fun setOuterBorderRadiusType(view: View, radius: Radius) {
    val context = view.context
    
    val cornerRadius = context.resources.getDimension(R.dimen.item_radius).dpToPx(context)

    val shape = GradientDrawable().apply {
        shape = GradientDrawable.RECTANGLE
        setColor(ContextCompat.getColor(context, R.color.purple_500))
        cornerRadii = floatArrayOf(
            // top left
            if (radius.topLeft) cornerRadius else 0f,
            if (radius.topLeft) cornerRadius else 0f,
            // top right
            if (radius.topRight) cornerRadius else 0f,
            if (radius.topRight) cornerRadius else 0f,
            // bottomRight
            if (radius.bottomRight) cornerRadius else 0f,
            if (radius.bottomRight) cornerRadius else 0f,
            // bottomLeft
            if (radius.bottomLeft) cornerRadius else 0f,
            if (radius.bottomLeft) cornerRadius else 0f,
        )
    }

    view.background = shape
}

@BindingAdapter("outerBackgroundMargin")
fun setOuterBackgroundMargin(view: View, marginType: Margin) {
    val params = view.layoutParams as FrameLayout.LayoutParams

    params.gravity = Gravity.NO_GRAVITY

    val dimen = view.context.resources.getDimension(R.dimen.item_border_gap_vertical).roundToInt()

    val marginTop = if (marginType.top) dimen else 0

    val marginLeft = if (marginType.left) dimen else 0

    val marginBottom = if (marginType.bottom) dimen else 0

    val marginRight = if (marginType.right) dimen else 0

    params.setMargins(marginLeft, marginTop, marginRight, marginBottom)
    view.layoutParams = params
}

@BindingAdapter("itemTypeBackground")
fun setItemTypeBackground(view: View, itemType: ItemType) {
    val backgroundRes = when (itemType) {
        ItemType.HIGH -> R.drawable.background_item_high
        ItemType.MEDIUM -> R.drawable.background_item_medium
        ItemType.DEVICE -> R.drawable.background_item_device
        ItemType.NONE -> R.drawable.background_item_none
    }

    view.setBackgroundResource(backgroundRes)
}

private fun Float.dpToPx(context: Context): Float {
    val r = context.resources
    return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, this, r.displayMetrics)
}
