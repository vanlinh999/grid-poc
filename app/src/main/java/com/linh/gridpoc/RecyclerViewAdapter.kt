package com.linh.gridpoc

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.linh.gridpoc.databinding.ItemGridBinding

class RecyclerViewAdapter : ListAdapter<GridItemUi, RecyclerViewAdapter.ViewHolder>(GridDiffUtil()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ItemGridBinding.inflate(layoutInflater, parent, false)

        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)

        holder.bind(item)
    }

    class ViewHolder(private val binding: ItemGridBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: GridItemUi) {
            binding.item = item
            binding.executePendingBindings()
        }
    }
}

class GridDiffUtil : DiffUtil.ItemCallback<GridItemUi>() {
    override fun areItemsTheSame(oldItem: GridItemUi, newItem: GridItemUi): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: GridItemUi, newItem: GridItemUi): Boolean {
        return oldItem == newItem
    }
}
