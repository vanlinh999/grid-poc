package com.linh.gridpoc

data class GridItemUi(
    val id: Int,
    val name: String,
    val location: String,
    val type: ItemType = ItemType.NONE,
    val margins: Margin = Margin(),
    val radius: Radius = Radius()
)

data class Radius(
    val topLeft: Boolean = false,
    val topRight: Boolean = false,
    val bottomLeft: Boolean = false,
    val bottomRight: Boolean = false
)

data class Margin(
    val bottom: Boolean = false,
    val top: Boolean = false,
    val left: Boolean = false,
    val right: Boolean = false
)

enum class ItemType {
    HIGH,
    MEDIUM,
    DEVICE,
    NONE
}
