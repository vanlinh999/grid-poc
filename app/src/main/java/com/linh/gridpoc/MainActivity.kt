package com.linh.gridpoc

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import com.linh.gridpoc.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private val rawMockData = listOf(
        GridItemUi(1, "BH1", "a", ItemType.HIGH),
        GridItemUi(2, "VA", "a", ItemType.NONE),
        GridItemUi(3, "VH", "a", ItemType.MEDIUM),
        GridItemUi(4, "PH", "a", ItemType.NONE),
        GridItemUi(5, "BH2", "a", ItemType.HIGH),
        GridItemUi(6, "JD", "a", ItemType.DEVICE),
        GridItemUi(7, "VH", "a", ItemType.MEDIUM),
        GridItemUi(8, "VH", "a", ItemType.MEDIUM),
        GridItemUi(9, "JD", "b", ItemType.DEVICE),
        GridItemUi(10, "JD", "c", ItemType.DEVICE),
        GridItemUi(11, "BH2", "c", ItemType.HIGH),
        GridItemUi(12, "VA", "c", ItemType.NONE),
        GridItemUi(13, "VA", "d", ItemType.NONE),
        GridItemUi(14, "VA", "d", ItemType.NONE),
        GridItemUi(15, "VA", "d", ItemType.NONE),
        GridItemUi(16, "VH", "e", ItemType.NONE),
        GridItemUi(17, "VH", "f", ItemType.NONE),
        GridItemUi(18, "VH", "g", ItemType.NONE),
        GridItemUi(19, "VH", "g", ItemType.NONE),
    )

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }

    override fun onStart() {
        super.onStart()

        val adapter = RecyclerViewAdapter()

        with(binding) {
            recyclerViewMain.adapter = adapter
            recyclerViewMain.layoutManager = GridLayoutManager(this@MainActivity, GRID_SPAN_COUNT)
        }

        val rowCount = rawMockData.size / GRID_SPAN_COUNT + 1

        val grid = Array(rowCount) {
            Array<GridItemUi?>(GRID_SPAN_COUNT) { null }
        }

        // Builds grid
        rawMockData.forEachIndexed { index, gridItemUi ->
            val gridRowIndex = index / GRID_SPAN_COUNT
            val gridColumnIndex = index % GRID_SPAN_COUNT

            grid[gridRowIndex][gridColumnIndex] = gridItemUi
        }

        // Read data from grid and calculate radius and margin
        val newList = mutableListOf<GridItemUi>()
        var previousItem: GridItemUi? = null
        grid.forEachIndexed { rowIndex, arrayOfGridItemUis ->
            arrayOfGridItemUis.forEachIndexed { columnIndex, gridItemUi ->
                gridItemUi?.let {
                    val radius = getItemRadius(
                        rowIndex,
                        columnIndex,
                        grid,
                        it,
                        previousItem,
                        gridItemUi,
                        rowCount
                    )
                    val margin =
                        getItemMargin(rowIndex, grid, columnIndex, gridItemUi, rowCount, it)

                    newList.add(
                        it.copy(
                            radius = radius,
                            margins = margin
                        )
                    )

                    previousItem = gridItemUi
                }
            }
        }

        adapter.submitList(newList)
    }

    private fun getItemMargin(
        rowIndex: Int,
        grid: Array<Array<GridItemUi?>>,
        columnIndex: Int,
        gridItemUi: GridItemUi,
        rowCount: Int,
        it: GridItemUi
    ): Margin {
        val topMargin =
            rowIndex != 0 && grid[rowIndex - 1][columnIndex]?.location != gridItemUi.location
        val bottomMargin =
            rowIndex < rowCount - 1 && grid[rowIndex + 1][columnIndex]?.location != gridItemUi.location
        val leftMargin =
            columnIndex != 0 && grid[rowIndex][columnIndex - 1]?.location != it.location
        val rightMargin =
            columnIndex < GRID_SPAN_COUNT - 1 && grid[rowIndex][columnIndex + 1]?.location != it.location
        return Margin(
            bottom = bottomMargin,
            top = topMargin,
            left = leftMargin,
            right = rightMargin
        )
    }

    private fun getItemRadius(
        rowIndex: Int,
        columnIndex: Int,
        grid: Array<Array<GridItemUi?>>,
        it: GridItemUi,
        previousItem: GridItemUi?,
        gridItemUi: GridItemUi,
        rowCount: Int
    ): Radius {
        val topLeftRadius = (rowIndex == 0 && columnIndex == 0) ||
            (columnIndex != 0 && grid[rowIndex][columnIndex - 1]?.location != it.location) ||
            (columnIndex == 0 && previousItem?.location != gridItemUi.location)
        val topRightRadius = (rowIndex == 0 && columnIndex == GRID_SPAN_COUNT - 1) ||
            (columnIndex == GRID_SPAN_COUNT - 1 && rowIndex < rowCount - 1 && grid[rowIndex - 1][columnIndex]?.location != gridItemUi.location) ||
            (columnIndex < GRID_SPAN_COUNT - 1 && grid[rowIndex][columnIndex + 1]?.location != it.location)
        val bottomLeftRadius = (rowIndex == rowCount - 1 && columnIndex == 0) ||
            (columnIndex == 0 && rowIndex < rowCount - 1 && grid[rowIndex + 1][columnIndex]?.location != gridItemUi.location) ||
            (rowIndex == rowCount - 1 && grid[rowIndex][columnIndex - 1]?.location != it.location) ||
            (columnIndex != 0 && grid[rowIndex][columnIndex - 1]?.location != it.location)
        val bottomRightRadius =
            (columnIndex < GRID_SPAN_COUNT - 1 && grid[rowIndex][columnIndex + 1]?.location != it.location) ||
                (columnIndex == GRID_SPAN_COUNT - 1 && rowIndex < rowCount - 1 && grid[rowIndex + 1][columnIndex]?.location != gridItemUi.location)
        return Radius(
            topLeft = topLeftRadius,
            topRight = topRightRadius,
            bottomLeft = bottomLeftRadius,
            bottomRight = bottomRightRadius
        )
    }

    companion object {
        private const val GRID_SPAN_COUNT = 4
    }
}
